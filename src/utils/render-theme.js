import { render } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import { Theme } from '../styles/theme';


export const renderTheme = (children) => {
  return render(<ThemeProvider theme={Theme}>{children}</ThemeProvider>);
};