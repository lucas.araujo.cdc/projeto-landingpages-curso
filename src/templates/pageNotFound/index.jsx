import P from 'prop-types'
import {GridContent} from "../../components/GridContent"


export const PageNotFound = () => {

    return <GridContent 
    title={'Erro404'}
    html="<p>A pagina que voce busca não pode ser encontrada.<a href='/'> Voltar para home<a/></p>"
    
    />
}

PageNotFound.prototype = {
children: P.node.isRequired,
}