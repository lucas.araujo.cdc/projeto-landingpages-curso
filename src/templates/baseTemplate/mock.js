import { mock } from '../../components/NavLinks/mock';


export const mockBase = {

  links: mock,
  logoData: {
    text: 'Logo',
    link: '#',
  },
  footerHtml: '<p>Teste de footer</p>',
};