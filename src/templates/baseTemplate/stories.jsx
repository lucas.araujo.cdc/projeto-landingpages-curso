import { BaseTemplate } from '.'

export default{
    title:'BaseTemplate',
    component : BaseTemplate,
    args:{
        children: 'BaseTemplate',
    },
    argTypes: {
        children: {
            ype: 'string'
        },
    },
    }

export const Template = (args)=> { 
    return ( 
            <div>
            <BaseTemplate {...args}/>
            </div>
    )
}