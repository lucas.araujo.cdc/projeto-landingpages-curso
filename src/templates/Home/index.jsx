import { useEffect, useState } from "react";
import { BaseTemplate } from "../baseTemplate";
import { mockBase } from "../baseTemplate/mock"
import {mapData } from "../../api/map-data"
import { PageNotFound } from "../pageNotFound";
export function Home() {


  const [data, setData] = useState([])
  useEffect(() => { 
    const load = async () => {
      try {
        const data = await fetch('http://localhost:1337/api/pages/?filters[slug]=landing-page&populate=deep') 
        const json = await data.json();
        console.log( json.data)
        const pageData  = mapData(json.data)
        setData(pageData[0])
        
      } catch (error) {
        setData(undefined)
      }
    }

    load()
  }, [])

  if(data === undefined){ 
    return <PageNotFound/>
  }
  if(data === !data.slug){ 
    return <h1>Carregando...</h1>
  }

  return (
    <BaseTemplate  tab="BaseTemplate " {...mockBase}/>
  );
}

