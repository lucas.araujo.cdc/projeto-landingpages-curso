import styled, { css } from 'styled-components';
import { Theme } from '../../styles/theme';

export const Container = styled.a`
    ${({theme}) => css`
        position: fixed;
        background: ${theme.colors.mediumGray};
        color: ${theme.colors.lightColor}
        display: flex;
        align-items: center;
        justify-content: center;
        width: 4rem;
        height: 4rem;
        bottom: 2rem;
        right: 2rem;
        opacity: 0.7;
        z-index: 6;
    `}
`
