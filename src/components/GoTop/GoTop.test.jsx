import { screen } from '@testing-library/react';
import { GoTop } from '.';
import { renderTheme } from '../../utils/render-theme'

describe('<GoTop />',()=>{
    it('should render', ()=>{
        renderTheme(<GoTop/>);
        expect(screen.getByRole('link', {name:'Go To Top'})).toBeInTheDocument()
        expect(screen.getByRole('link', {name:'Go To Top'})).toHaveAttribute('href', "#")
    })

})