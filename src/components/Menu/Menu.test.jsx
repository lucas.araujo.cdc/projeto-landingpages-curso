import { screen } from '@testing-library/react';
import { Menu } from '.';
import { renderTheme } from '../../utils/render-theme'
import {Theme} from '../../styles/theme'

import { mock } from"../NavLinks/mock"
const logoData = { 
    text: 'Logo',
    link: '#target'
}

describe('<Menu />',()=>{
    it('should render', ()=>{
        const { container } = renderTheme(<Menu  links={mock} logoData={logoData}/>);
        expect(screen.getByRole('link', { name: 'Logo' })).toBeInTheDocument();
        expect(screen.getByRole('navigation',{name: "Main menu"})).toBeInTheDocument();
        // expect(container).toMatchSnapshot();
    })


    it('should render menu mobile and bottom for open and close the menu', ()=>{
        const { container } = renderTheme(<Menu  links={mock} logoData={logoData}/>);
        const button = screen.getByLabelText('Open/Close menu')
        const menuContainer = button.nextSibling;
        
        
        expect(button).toHaveStyleRule('display', 'none');
        // expect(button).toHaveStyleRule('display', 'flex', {
        //   media: Theme.media.lteMedium,
        // });
        // expect(menuContainer).toHaveStyleRule('opacity', '1', {
        //   media: Theme.media.lteMedium,
        // });
        expect(screen.getByLabelText("Main menu")).toBeInTheDocument();


    })

    it('should not render links', ()=>{
        const { container } = renderTheme(<Menu logoData={logoData}/>);
        expect(screen.queryByRole('navigation',{name: "Main menu"}).firstChild,).not.toBeInTheDocument();
        expect(container ).toMatchSnapshot()
        
    })
})