import styled, { css } from 'styled-components';

const containerBackgroundActivate = (theme) => css`
        background: ${theme.colors.prymaryColor};
        color: ${theme.colors.lightColor};
`

export const Container = styled.div`
    ${({theme, background}) => css`
        background: ${theme.colors.lightColor};
        color: ${theme.colors.prymaryColor};
        ${background && containerBackgroundActivate(theme)}
        min-height:100vh;
        display: flex;
        align-items: center;
        justify-content: center;
    `}
`
