import { SectionBackground } from '.'

export default{
    title:'SectionBackground',
    component : SectionBackground,
    args:{
        children: 'SectionBackground',
    },
    argTypes: {
        children: {
            ype: 'string'
        },
    },
    }

export const Template = (args)=> { 
    return ( 
            <div>
            <SectionBackground {...args}/>
            </div>
    )
}