import { MenuLink } from '.'

export default{
    title:'MenuLink',
    component : MenuLink,
    args:{
        children: 'MenuLink',
        link: 'https://www.google.com.br'
    },
    argTypes: {
        children: {
            Type: 'string',
            link:'string'

        },

    },
    }

export const Template = (args)=> { 
    return ( 
            <div>
                <MenuLink {...args}/>
            </div>
    )
}