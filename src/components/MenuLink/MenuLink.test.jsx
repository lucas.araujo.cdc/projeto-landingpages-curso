import { screen} from '@testing-library/react';
import { MenuLink } from '.';
import { renderTheme } from '../../utils/render-theme'


describe('<MenuLink />',()=>{
    it('should render', ()=>{
        renderTheme(<MenuLink link="https://localhost">Children</MenuLink>);
        expect(screen.getByRole('link', {name:"Children" })).toHaveAttribute("target", "_self")
    })

    it('should render at a new tab', ()=>{
        renderTheme(<MenuLink link="https://localhost" newTab={true}>Children</MenuLink>);
        expect(screen.getByRole('link', {name:"Children" })).toHaveAttribute("target", "_blank")
    })    
    
    // it('google', async()=>{
    //     renderTheme(<MenuLink link="https://google.com" newTab={false}>Children</MenuLink>);
    //     const linkElement = screen.getByText(/Children/i);
    //     fireEvent.click(linkElement)
        
    //     await expect().toBe()
    // })

    // it('should render snapShot', ()=>{
    //     renderTheme(<MenuLink link="https://localhost" newTab={false} >Children</MenuLink>);
    //     expect(screen.getByRole('link', {name:"Children" })).toMatchInlineSnapshot();
    // })

})