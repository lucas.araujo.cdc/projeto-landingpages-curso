import { SectionContainer } from '.'

export default{
    title:'SectionContainer',
    component : SectionContainer,
    args:{
        children: 'SectionContainer',
    },
    argTypes: {
        children: {
            ype: 'string'
        },
    },
    }

export const Template = (args)=> { 
    return ( 
            <div>
            <SectionContainer {...args}/>
            </div>
    )
}