import styled, { css } from "styled-components"
import { Theme } from "../../styles/theme";


const titleSizes = { 
    small: (theme)=> css`
        font-size: ${theme.font.size.medium};
    `,
    medium: (theme)=> css`
        font-size: ${theme.font.size.large};
    `,
    big: (theme)=> css`
        font-size: ${theme.font.size.xlarge};
    `,
    huge: (theme)=> css`
        font-size: ${theme.font.size.huge};
    `
}

const titleCase = (uppercase) => css`
    text-transform: ${uppercase?"uppercase":"none"}; 
    ${mediaFont(Theme)};
`;

const mediaFont = (theme)=> css`
    @media ${theme.media.lteMedium} {
        font-size: ${theme.font.size.xlarge};
    }
`
export const Title = styled.h1`
    ${({theme, colorDark, size, uppercase}) => css`
    color:${colorDark? theme.colors.lightColor:theme.colors.prymaryColor};
    ${titleSizes[size](theme)}
    ${titleCase(uppercase)}
    
    `}
`;