import {Heading} from '.'

export default{
    title:'Heading',
    component : Heading,
    args:{
        children: "Nao sei2",
    },
    argTypes: {
        children: {
            type: "string"
        },
    },

    backgrounds: {
        default: 'dark'
    },
}

export const Light = (args) => <Heading {...args} />
export const dark = (args) => <Heading {...args} />

Light.parameters={
    backgrounds:{
        default: 'light'
    }
}

dark.args = {
    children: "Nao sei1",
    colorDark: false,
}