import { screen } from "@testing-library/react";
import { Heading } from ".";
import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import React from "react";
import { renderTheme } from "../../utils/render-theme";


describe("Heading", ()=> {

    it('should render with default values', ()=> {
        renderTheme(<Heading >texto</Heading>)
        const heading = screen.getByRole('heading', {name:'texto'});
        expect(heading).toHaveStyle({
            color: Theme.colors.prymaryColor,
            "font-size": Theme.font.size.xlarge,
            "text-transform": "none",
        })

    });    
    
    it('should render with white color values', ()=> {
        renderTheme(<Heading colorDark >texto</Heading>)
        const heading = screen.getByRole('heading', {name:'texto'});
        expect(heading).toHaveStyle({
            color: Theme.colors.lightColor,
            

        })

    });

    it('should render with size huge', ()=> {
        const { rerender } = renderTheme(<Heading size="huge" >texto</Heading>)
        const heading = screen.getByRole('heading', {name:'texto'});
        expect(heading).toHaveStyle({
            "font-size": Theme.font.size.huge,
        })

        rerender(
            <ThemeProvider theme={Theme}>
                <Heading size="huge">texto</Heading>
            </ThemeProvider>
        )

        expect(heading).toHaveStyle({
            "font-size": Theme.font.size.huge,
        })

        rerender(
            <ThemeProvider theme={Theme}>
                <Heading size="big">texto</Heading>
            </ThemeProvider>
        )

        expect(heading).toHaveStyle({
            "font-size": Theme.font.size.xlarge,
        })

    });


    it('should render when in uppercase', ()=> {
        renderTheme(<Heading uppercase={true}>texto</Heading>)
        const heading = screen.getByRole('heading', {name:'texto'});
        expect(heading).toHaveStyle({

            'text-transform': 'uppercase',
        })

    });    
    
    it('should render correct heading element', ()=> {
        const { container } = renderTheme(<Heading as="h6">texto</Heading>)
        const heading = screen.getByRole('heading', {name:'texto'});
        const h6 = container.querySelector("h6");
        expect(h6.tagName.toLowerCase()).toBe("h6")
    });
});