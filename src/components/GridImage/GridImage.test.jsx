import { screen } from '@testing-library/react';
import { GridImage } from '.';
import { renderTheme } from '../../utils/render-theme'
import { GridImageMock } from './mock';

describe('<GridImage />',()=>{
    it('should render', ()=>{
        const { container } = renderTheme(<GridImage {...GridImageMock}/>);
        expect(container).toMatchSnapshot()
    })
})