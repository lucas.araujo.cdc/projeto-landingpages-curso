import { GridText } from '.'

export default{
    title:'GridText',
    component : GridText,
    args:{
        children: 'GridText',
    },
    argTypes: {
        children: {
            ype: 'string'
        },
    },
    }

export const Template = (args)=> { 
    return ( 
            <div>
            <GridText {...args}/>
            </div>
    )
}