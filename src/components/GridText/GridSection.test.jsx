import { screen } from '@testing-library/react';
import { GridSection } from '.';
import { renderTheme } from '../../utils/render-theme'
import { GridSectionMock } from './mock';

describe('<GridSection />',()=>{
    it('should render', ()=>{
        const {container}= renderTheme(<GridSection {...GridSectionMock}/>);
        expect(container).toMatchSnapshot()
    })
    it('should render whit background undefined', ()=>{
        const {container}= renderTheme(<GridSection {...GridSectionMock} background={undefined}/>);
        expect(container).toMatchSnapshot()
    })
})