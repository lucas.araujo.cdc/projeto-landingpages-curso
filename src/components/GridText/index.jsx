import P from 'prop-types'
import * as Styled from './styles'
import {SectionBackground} from "../SectionBackground"
import {Heading} from "../Heading"
import {Text} from "../TextComponent"

export const GridText = ({title, description, grid, background=false}) => {

    return(
        <SectionBackground  background={background}> 
            <Styled.Container>
                <Heading size="huge" colorDark={background} as="h2" uppercase>{title}</Heading>
                <Text>{description}</Text>
                <Styled.Grid>
                    {grid.map((el)=>(
                        <Styled.GridElement key={el.title}>
                            <Heading size="medium" colorDark={background} as="h3" >{el.title}</Heading> 
                            <Text>{el.description}</Text>
                        </Styled.GridElement>
                    ))}
                </Styled.Grid>
            </Styled.Container>
        </SectionBackground>
    )
}

GridText.prototype = {
    title: P.node.isRequired,
    description: P.node.isRequired,
    background: P.bool,
    grid: P.arrayOf( 
        P.shape({
            title: P.node.isRequired,
            description: P.node.isRequired,
        }).isRequired,
    )
}