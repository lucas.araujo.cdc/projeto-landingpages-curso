import { Footer } from '.';
import { renderTheme } from '../../utils/render-theme'

describe('<Footer />',()=>{
    it('should render', ()=>{
        const {container} = renderTheme(<Footer footerHtml="<h1>Testando o footer para ver se nao quebra aki</h1>" /> );
        expect(container).toMatchInlineSnapshot();
    })
})