import P from 'prop-types'
import * as Styled from './styles'
import {Heading} from "../Heading/index"

export const LogoLink = ({text, srcImg, link}) => {

    return(
    <Heading size="small" uppercase colorDark={false} >
        <Styled.Container href={link}>
          {!!srcImg && <img src={srcImg} alt={text} />}
          {!srcImg && text}
        </Styled.Container>
      </Heading>
    )
}

LogoLink.prototype = {
    children: P.node.isRequired,
    srcImg: P.string,
    link: P.string.isRequired
}