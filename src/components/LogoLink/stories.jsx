import { LogoLink } from '.'

export default{
    title:'LogoLink',
    component : LogoLink,
    args:{
        children: 'LogoLink',
    },
    argTypes: {
        text: 'LogoLink',
        srcImg: 'assets/images/logo.svg',
        link: 'http://localhost',
      },
    }

export const Template = (args)=> { 
    return ( 
            <div>
            <LogoLink {...args}/>
            </div>
    )
}