import { screen } from '@testing-library/react';
import { GridContent } from '.';
import { renderTheme } from '../../utils/render-theme'
import { gridMock } from './mock';

describe('<GridContent />',()=>{
    it('should render', ()=>{
        const { container } = renderTheme(<GridContent {...gridMock}/>);
        expect(container) = toMatchSnapshot();
    })
    it('should render whit  background false ', ()=>{
        const { container } = renderTheme(<GridContent {...gridMock} background={undefined}/>);
        expect(container) = toMatchSnapshot();
    })
})