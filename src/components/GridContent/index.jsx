import P from 'prop-types'
import * as Styled from './styles'
import { SectionBackground } from '../SectionBackground'
import { Heading } from '../Heading'
import { Text } from "../TextComponent"

export const GridContent = ({title, html, background = false}) => {

    return(

        <SectionBackground background={background}>
            <Styled.Container>
                <Heading uppercase colorDark={background} as='h2'>{title}</Heading>
                <Text>{html}</Text>
            </Styled.Container>
        </SectionBackground>
    )
}

GridContent.prototype = {
children: P.node.isRequired,
}