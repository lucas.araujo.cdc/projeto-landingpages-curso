import { GridContent } from '.'
import { gridMock } from "../GridContent"
export default{
    title:'GridContent',
    component : GridContent,
    args: gridMock,
    }

export const Template = (args)=> { 
    return ( 
        <div>
            <GridContent {...args}/>
        </div>
    )
}