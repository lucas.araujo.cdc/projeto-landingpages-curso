import { Text } from '.'

export default{
    title:'Text',
    component : Text,
    args:{
        children:`Lorem ipsum dolor sit amet consectetur adipisicing elit. 
        Odit alias et dignissimos distinctio nesciunt expedita totam quas, 
        doloremque sunt itaque, mollitia aperiam, fuga id? Iste ipsam provident explicabo a error.`,
    },
    argTypes: {
        children: {
            ype: 'string'
        },
    },
    }

export const Template = (args)=> { 
    return ( 
            <div>
            <Text {...args}/>
            </div>
    )
}