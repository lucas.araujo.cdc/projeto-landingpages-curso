import { screen } from '@testing-library/react';
import { Text } from '.';
import { renderTheme } from '../../utils/render-theme'

describe('<Text />',()=>{

    it('should render', ()=>{
        const {container } =renderTheme(<Text>Children</Text>);
        expect(screen.getByRole("heading")).toBeInTheDocument()

    })    
    

})