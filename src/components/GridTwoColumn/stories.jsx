import { GridTwoColumn } from '.'

export default{
    title:'GridTwoColumn',
    component : GridTwoColumn,
    args:{
        children: 'GridTwoColumn',
    },
    argTypes: {
        children: {
            ype: 'string'
        },
    },
    }

export const Template = (args)=> { 
    return ( 
            <div>
            <GridTwoColumn {...args}/>
            </div>
    )
}