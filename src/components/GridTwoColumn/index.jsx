import P from 'prop-types'
import * as Styled from './styles'
import { SectionBackground } from "../SectionBackground"
import {Heading} from '../Heading'
import {Text} from "../TextComponent"
export const GridTwoColumn = ({title, text, srcImg, background = false}) => {

    return(
        <SectionBackground background={background}> 
            <Styled.Container>
                <Styled.TextContainer>
                    <Heading uppercase>{title}</Heading>
                    <Text>{text}</Text>
                </Styled.TextContainer>               
                <Styled.ImageContainer >
                   < Styled.Image src={srcImg} alt={title}/>
                </Styled.ImageContainer>
            </Styled.Container>
        </SectionBackground> 
    )
}

GridTwoColumn.prototype = {
title: P.string.isRequired, 
text: P.string.isRequired, 
srcImg: P.string.isRequired, 
background: P.bool,
}