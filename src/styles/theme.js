export const Theme = {

    colors:
    {
        prymaryColor:`#0A1128`,
        secundaryColor: `#dc143c`,
        lightColor: `#fff`,
        darkColor: `#0A1128`,
        mediumGray: `#DDDDDD`,
        white: `#fff`,
    },

    font:{

        size:{
            xsmal:`8rem`,
            small: `1.6rem`,
            medium:`2.4rem` ,
            large:`3.2rem` ,
            xlarge:`4.0rem` ,
            xxlarge:`4.8rem` ,
            huge:`5.6rem` ,
            xhuge:`6.4rem` 
        },
    },    
    
    
    media:{
        lteMedium: "(max-width: 768px)"
    },
    
    spacings:{
        xsmal:`8rem`,
        small: `1.6rem`,
        medium:`2.4rem` ,
        large:`3.2rem` ,
        xlarge:`4.0rem` ,
        xxlarge:`4.8rem` ,
        huge:`5.6rem` ,
        xhuge:`6.4rem` 
    },
}