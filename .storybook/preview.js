import { ThemeProvider } from 'styled-components';
import { Theme } from '../src/styles/theme';
import { GlobalStyle } from '../src/styles/global-styles';

/** @type { import('@storybook/react').Preview } */
const preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
    backgrounds:{
      default: 'light',
      values: [
        {
          name:'light',
          value: Theme.colors.lightColor

        },     
        {
          name:'dark',
          value: Theme.colors.darkColor
        },
      ]
    }
  },

};


export const decorators =[
  (Story)=>(
    <ThemeProvider theme={Theme}>
      <Story/>
      <GlobalStyle/>
    </ThemeProvider>
  )
]
export default preview;
